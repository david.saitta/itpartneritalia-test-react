import React from "react";
import Select from "react-select";
import CountriesApi from "../dataset/CountriesApi";

class PersonInspectPanel extends React.Component {
  render() {
    const {
      gender,
      name,
      location,
      email,
      login,
      dob,
      registered,
      phone,
      cell,
      id,
      picture,
      nat
    } = this.props.person;

    return (
      <div style={{ position: "sticky", top: "1em" }}>
        <img
          src={picture.medium}
          className="img-thumbnail rounded mx-auto d-block"
        />
        <ol>
          <dt>Email</dt>
          <dl>{email}</dl>
          <dt>Nome</dt>
          <dl>
            {name.title} {name.first} {name.last}
          </dl>
          <dt>Country</dt>
          <dl>
            <CountriesApi>
              {data => {
                const options = data.map(item => ({
                  value: item.alpha2Code,
                  label: item.name
                }));
                const selectedValue = options.filter(
                  option => option.value === nat
                );

                return <Select options={options} value={selectedValue} />;
              }}
            </CountriesApi>
          </dl>
        </ol>
      </div>
    );
  }
}

export default PersonInspectPanel;

import React from "react";
import PeopleDataGrid from "./PeopleDataGrid";
import { Row, Col, Alert } from "reactstrap";
import PersonInspectPanel from "./PersonInspectPanel";

class MainPage extends React.Component {
  state = {
    selectedRow: null
  };

  constructor(props) {
    super(props);
  }

  onSelectionChanged(selectedRows) {
    let row = selectedRows[0];
    this.setState({ selectedRow: row });
  }

  render() {
    const selectedRow = this.state.selectedRow;
    return (
      <React.Fragment>
        <Row>
          <Col>
            <small>
              Shortcut globali (no focus sulla griglia): <i>ctrl+alt+s</i>
              <br />
              Shortcut griglia: <i>ctrl+s</i>, <i>ctrl+m</i>
              <br />
              ctrl+f: Focus sul primo campo di filtro
            </small>
          </Col>
        </Row>
        <Row className="mt-4">
          <Col xs="9">
            <PeopleDataGrid
              onSelectionChanged={this.onSelectionChanged.bind(this)}
            />
          </Col>
          <Col xs="3">
            {selectedRow && <PersonInspectPanel person={selectedRow} />}
            {!selectedRow && <Alert color="info">Seleziona una riga</Alert>}
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default MainPage;

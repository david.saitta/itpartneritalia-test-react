import React from "react";
import KeyCode from "keycode-js";

export const withGlobalKeyboardShortcuts = Component => {
  class GlobalKeyboardShortcutsHandler extends React.Component {
    componentWillMount() {
      document.addEventListener("keydown", this.handleKeyDown.bind(this));
    }

    componentWillUnmount() {
      document.removeEventListener("keydown", this.handleKeyDown.bind(this));
    }

    handleKeyDown(event) {
      let { key, keyCode, metaKey, altKey, ctrlKey } = event;
      console.debug("global keydown", key, metaKey, altKey, ctrlKey, event);
      //prevent default browser shortcuts
      if ((ctrlKey || metaKey) && keyCode == KeyCode.KEY_S) {
        event.preventDefault();
      }

      if (ctrlKey && altKey && keyCode == KeyCode.KEY_S) {
        event.preventDefault(); //stop event propagation
        alert(`ctrl+alt+s page level`);
      }

      if (ctrlKey && keyCode == KeyCode.KEY_F) {
        //focus on the first filter
        let filterInput = document.querySelector(
          '[ref="eColumnFloatingFilter"]'
        );
        if (filterInput) filterInput.focus();
      }
    }

    render() {
      return <Component {...this.props} />;
    }
  }

  GlobalKeyboardShortcutsHandler.displayName = `withGlobalKeyboardShortcuts(${Component.displayName ||
    Component.name})`;

  return GlobalKeyboardShortcutsHandler;
};

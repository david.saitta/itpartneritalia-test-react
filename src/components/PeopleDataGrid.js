import React from "react";
import { Modal, ModalHeader, ModalBody, Button, ModalFooter } from "reactstrap";
import MainDataGrid from "./MainDataGrid";
import PeopleApi from "../dataset/PeopleApi";
import KeyCode from "keycode-js";

class PeopleDataGrid extends React.Component {
  gridApi;

  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      selectedData: {},
      columnDefs: [
        {
          headerName: "Nome",
          filter: "agTextColumnFilter",
          field: "name.first"
        },
        {
          headerName: "Cognome",
          filter: "agTextColumnFilter",
          field: "name.last"
        },
        {
          headerName: "Sesso",
          filter: "agTextColumnFilter",
          field: "gender"
        },
        {
          headerName: "Età",
          filter: "agNumberColumnFilter",
          field: "dob.age"
        },
        {
          headerName: "Email",
          filter: "agTextColumnFilter",
          field: "email"
        },
        {
          headerName: "Città",
          filter: "agTextColumnFilter",
          field: "location.city"
        }
      ],
      defaultColDef: {
        sortable: true,
        filter: true,
        resizable: true
      },
      rowSelection: "single",
      isExternalFilterPresent: this.isExternalFilterPresent.bind(this),
      doesExternalFilterPass: this.doesExternalFilterPass.bind(this),
      minAgeFilterValue: null,
      maxAgeFilterValue: null
    };
  }

  // register GridApi reference on load
  registerGridApi(e) {
    this.gridApi = e.api;
  }

  // Custom external grid filter
  filterDataByMinAge(e) {
    let value = parseInt(e.target.value, 10);
    if (value < 0 || isNaN(value)) value = null;

    this.setState({ minAgeFilterValue: value }, () => {
      this.gridApi.onFilterChanged();
    });
  }

  filterDataByMaxAge(e) {
    let value = parseInt(e.target.value, 10);
    if (value < 0 || isNaN(value)) value = null;

    this.setState({ maxAgeFilterValue: value }, () => {
      this.gridApi.onFilterChanged();
    });
  }

  // Called by the grid to check if filter the results with external filter
  isExternalFilterPresent() {
    return this.state.minAgeFilterValue || this.state.maxAgeFilterValue;
  }

  // Filter data based on external filter value
  doesExternalFilterPass(node) {
    const minAgeValue = this.state.minAgeFilterValue;
    const maxAgeValue = this.state.maxAgeFilterValue;

    if (minAgeValue > 0 && maxAgeValue > 0)
      return (
        node.data.dob.age >= minAgeValue && node.data.dob.age <= maxAgeValue
      );

    if (minAgeValue > 0) return node.data.dob.age >= minAgeValue;
    if (maxAgeValue > 0) return node.data.dob.age <= maxAgeValue;

    return true;
  }

  handleKeyboardShurtcuts(e) {
    let { keyCode, ctrlKey } = e.event;
    let rowNode = e.node;
    let rowData = rowNode.data;

    if (ctrlKey && keyCode == KeyCode.KEY_M) {
      this.setState({ selectedData: rowData });
      this.modalToggle();
    }
  }

  modalToggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  getContextMenuItems(params) {
    console.debug(params);
    var result = [
      "autoSizeAll",
      {
        name: "Alert " + params.value,
        action: function() {
          window.alert("Alerting about " + params.value);
        },
        cssClasses: ["redFont", "bold"]
      },
      {
        name: "Menu 1",
        subMenu: [
          {
            name: "Submenu 1",
            action: function() {
              alert("Submenu 1 was pressed");
            }
          },
          {
            name: "Submenu 2",
            action: function() {
              alert("Submenu 2 was pressed");
            }
          }
        ]
      },
      "separator",
      "export",
      "copy"
    ];
    return result;
  }

  render() {
    return (
      <React.Fragment>
        Min age:&nbsp;
        <input
          type="text"
          name="min"
          onChange={this.filterDataByMinAge.bind(this)}
        />
        Max age:&nbsp;
        <input
          type="text"
          name="max"
          onChange={this.filterDataByMaxAge.bind(this)}
        />
        <PeopleApi>
          {data => (
            <MainDataGrid
              ref="mainDataGrid"
              rowData={data}
              columnDefs={this.state.columnDefs}
              defaultColDef={this.state.defaultColDef}
              rowSelection={this.state.rowSelection}
              onSelectionChanged={this.props.onSelectionChanged}
              onCellKeyDown={this.handleKeyboardShurtcuts.bind(this)}
              onGridReady={this.registerGridApi.bind(this)}
              isExternalFilterPresent={this.state.isExternalFilterPresent}
              doesExternalFilterPass={this.state.doesExternalFilterPass}
              allowContextMenuWithControlKey={true}
              getContextMenuItems={this.getContextMenuItems}
            />
          )}
        </PeopleApi>
        <Modal
          isOpen={this.state.modal}
          toggle={this.modalToggle.bind(this)}
          className={this.props.className}
        >
          <ModalHeader toggle={this.modalToggle.bind(this)}>
            {this.state.selectedData.email}
          </ModalHeader>
          <ModalBody>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
            aliquip ex ea commodo consequat. Duis aute irure dolor in
            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
            pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
            culpa qui officia deserunt mollit anim id est laborum.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={this.modalToggle.bind(this)}>
              Do Something
            </Button>{" "}
            <Button color="secondary" onClick={this.modalToggle.bind(this)}>
              Cancel
            </Button>
          </ModalFooter>
        </Modal>
      </React.Fragment>
    );
  }
}

export default PeopleDataGrid;

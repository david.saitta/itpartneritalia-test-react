import React, { Component } from "react";
import { AgGridReact } from "ag-grid-react";

class MainDataGrid extends Component {
  navigateToNextCell(params) {
    let previousCell = params.previousCellDef;
    let suggestedNextCell = params.nextCellDef;
    let gridApi = this.refs.dataGrid.api;

    const KEY_UP = 38;
    const KEY_DOWN = 40;
    const KEY_LEFT = 37;
    const KEY_RIGHT = 39;

    switch (params.key) {
      case KEY_DOWN:
        previousCell = params.previousCellDef;
        // set selected cell on current cell + 1
        gridApi.forEachNode(node => {
          if (previousCell.rowIndex + 1 === node.rowIndex) {
            node.setSelected(true);
          }
        });
        return suggestedNextCell;
      case KEY_UP:
        previousCell = params.previousCellDef;
        // set selected cell on current cell - 1
        gridApi.forEachNode(node => {
          if (previousCell.rowIndex - 1 === node.rowIndex) {
            node.setSelected(true);
          }
        });
        return suggestedNextCell;
      case KEY_LEFT:
      case KEY_RIGHT:
        return suggestedNextCell;
      default:
        throw "this will never happen, navigation is always one of the 4 keys above";
    }
  }

  onSelectionChanged({ api, columnApi, type }) {
    let selectedRows = api.getSelectedRows();
    const callback = this.props.onSelectionChanged;
    if (callback) callback(selectedRows);
  }

  handleKeyboardShurtcuts(e) {
    let { key, metaKey, altKey, ctrlKey } = e.event;
    let rowNode = e.node;
    let rowData = rowNode.data;

    if (ctrlKey && key == "s") {
      e.event.preventDefault();
      alert(`ctrl+s on ${rowData.name.first} ${rowData.name.last}`);
    }

    if (metaKey && key == "s") {
      e.event.preventDefault();
      alert(`meta+s on ${rowData.name.first} ${rowData.name.last}`);
    }

    if (this.props.onCellKeyDown) this.props.onCellKeyDown(e);
  }

  focusOnTheFirstRow(event) {
    let { type, api, columnApi } = event;
    const firstRowIndex = api.getFirstDisplayedRow();
    let firstRow = api.getDisplayedRowAtIndex(firstRowIndex);
    if (firstRow) firstRow.setSelected(true);
  }

  focusOnTheClickedRow(event) {
    let { node } = event;
    node.setSelected(true);
  }

  render() {
    return (
      <div className="ag-theme-balham">
        <AgGridReact
          ref="dataGrid"
          floatingFilter={true}
          {...this.props}
          onCellKeyDown={this.handleKeyboardShurtcuts.bind(this)}
          onSelectionChanged={this.onSelectionChanged.bind(this)}
          navigateToNextCell={this.navigateToNextCell.bind(this)}
          onModelUpdated={this.focusOnTheFirstRow.bind(this)}
          onCellContextMenu={this.focusOnTheClickedRow.bind(this)}
        />
      </div>
    );
  }
}

export default MainDataGrid;

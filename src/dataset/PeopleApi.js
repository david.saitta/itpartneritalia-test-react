import React from "react";
import PropTypes from "prop-types";

class PeopleApi extends React.Component {
  controller = new AbortController();

  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  componentDidMount() {
    fetch("https://randomuser.me/api/?results=100", {
      signal: this.controller.signal
    })
      .then(results => results.json())
      .then(data => this.setState({ data: data.results }));
  }

  componentWillUnmount() {
    this.controller.abort();
  }

  render() {
    const { data } = this.state;
    const { children } = this.props;

    return children(data);
  }
}

PeopleApi.propTypes = {
  children: PropTypes.func.isRequired
};

export default PeopleApi;

import React from "react";
import PropTypes from "prop-types";

class CountriesApi extends React.Component {
  controller = new AbortController();

  constructor(props) {
    super(props);

    this.state = {
      data: []
    };
  }

  componentDidMount() {
    fetch("https://restcountries.eu/rest/v2/all", {
      signal: this.controller.signal
    })
      .then(results => results.json())
      .then(data => this.setState({ data: data }));
  }

  componentWillUnmount() {
    this.controller.abort();
  }

  render() {
    const { data } = this.state;
    const { children } = this.props;

    return children(data);
  }
}

CountriesApi.propTypes = {
  children: PropTypes.func.isRequired
};

export default CountriesApi;

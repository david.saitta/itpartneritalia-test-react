# IT Partner Italia - REACT POC

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Deployment

Gitlab CI/CD pipelane that deploy on each push on master. http://david.saitta.gitlab.io/itpartneritalia-test-react/

# POC

## External Components

- ag-Grid: data grid https://www.ag-grid.com.
- react-select: "combo box" https://react-select.com
- reactstrap: react bootstrap 4 components

## External API

- People API: https://randomuser.me
- Countries API: https://restcountries.eu

## Components

### DataSet HOC

- PeopleApi: loads people dummy data
- CountriesApi: loads list of countries

### Structure

```
App.js
  +
  +---+ GlobalKeyboardShortcutsHandler
  +---+ MainPage
          +
          +---+ PeopleDataGrid
          +           +
          +           +---+ PeopleApi
          +                    +
          +                    +---+ MainDataGrid
          +                    +---+ Modal
          +
          +---+ PersonInspectPanel
                      +
                      +---+ CountriesApi
                                +
                                +---+ Select
```
